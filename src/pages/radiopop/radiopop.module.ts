import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RadiopopPage } from './radiopop';

@NgModule({
  declarations: [
    RadiopopPage,
  ],
  imports: [
    IonicPageModule.forChild(RadiopopPage),
  ],
})
export class RadiopopPageModule {}
