import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-radiopop',
  templateUrl: 'radiopop.html',
})
export class RadiopopPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RadiopopPage');
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

}
