import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-new',
  templateUrl: 'new.html',
})
export class NewPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewPage');
  }

  radiopop(){
      let modal = this.modalCtrl.create('RadiopopPage');
      modal.present();
    }

  goBack(){
 this.navCtrl.pop();
}

}
