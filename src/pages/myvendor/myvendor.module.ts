import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyvendorPage } from './myvendor';

@NgModule({
  declarations: [
    MyvendorPage,
  ],
  imports: [
    IonicPageModule.forChild(MyvendorPage),
  ],
})
export class MyvendorPageModule {}
