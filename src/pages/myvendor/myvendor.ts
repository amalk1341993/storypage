import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,Content,Slides,ModalController,PopoverController } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-myvendor',
  templateUrl: 'myvendor.html',
})
export class MyvendorPage {
  showPrev:any;
  showNext:any;
  currentIndex:any;
  slidertab:any;
  @ViewChild(Content) content: Content;
  @ViewChild('mySlider') slider: Slides;
  constructor(public navCtrl: NavController, public navParams: NavParams,public popoverCtrl: PopoverController,public modalCtrl: ModalController) {
    let id = this.navParams.get("id");
    this.slidertab = id;
    console.log("id", id);
    setTimeout(() => {
    this.goToSlide(id);
    }, 500)
    this.slidertab = 0;
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad MyvendorPage');
  }

  goToSlide(id) {
  this.slider.slideTo(id, 500);
  }

  slideChanged() {
  let currentIndex = this.slider.getActiveIndex();
  this.slidertab = currentIndex;
  console.log("Current index is", currentIndex);
  }
  goBack(){
 this.navCtrl.pop();
}
  presentPopover(myEvent) {
   let popover = this.popoverCtrl.create('EventpopoverPage');
   popover.present({
     ev: myEvent
   });
 }

 addvendor(){
     let modal = this.modalCtrl.create('AddvendorPage');
     modal.present();
   }

   vendordetail(){
       let modal = this.modalCtrl.create('VendordetailPage');
       modal.present();
     }



}
