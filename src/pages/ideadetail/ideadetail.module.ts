import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IdeadetailPage } from './ideadetail';

@NgModule({
  declarations: [
    IdeadetailPage,
  ],
  imports: [
    IonicPageModule.forChild(IdeadetailPage),
  ],
})
export class IdeadetailPageModule {}
