import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-ideadetail',
  templateUrl: 'ideadetail.html',
})
export class IdeadetailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IdeadetailPage');
  }

  goBack(){
 this.navCtrl.pop();
}

}
