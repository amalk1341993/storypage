import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultdetailPage } from './resultdetail';

@NgModule({
  declarations: [
    ResultdetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultdetailPage),
  ],
})
export class ResultdetailPageModule {}
