import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-resultdetail',
  templateUrl: 'resultdetail.html',
})
export class ResultdetailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultdetailPage');
  }

  goBack(){
 this.navCtrl.pop();
}

moreinfo(){
    let modal = this.modalCtrl.create('MoreinfomodalPage');
    modal.present();
  }

  filter(){
      let modal = this.modalCtrl.create('FilterbyPage');
      modal.present();
    }

}
