import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-coveredit',
  templateUrl: 'coveredit.html',
})
export class CovereditPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CovereditPage');
  }

  goBack(){
 this.navCtrl.pop();
}

}
