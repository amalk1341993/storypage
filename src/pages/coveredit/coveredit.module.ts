import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CovereditPage } from './coveredit';

@NgModule({
  declarations: [
    CovereditPage,
  ],
  imports: [
    IonicPageModule.forChild(CovereditPage),
  ],
})
export class CovereditPageModule {}
