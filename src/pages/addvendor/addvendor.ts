import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-addvendor',
  templateUrl: 'addvendor.html',
})
export class AddvendorPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddvendorPage');
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

}
