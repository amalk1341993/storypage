import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormfilterPage } from './formfilter';

@NgModule({
  declarations: [
    FormfilterPage,
  ],
  imports: [
    IonicPageModule.forChild(FormfilterPage),
  ],
})
export class FormfilterPageModule {}
