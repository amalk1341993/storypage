import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VendordetailPage } from './vendordetail';

@NgModule({
  declarations: [
    VendordetailPage,
  ],
  imports: [
    IonicPageModule.forChild(VendordetailPage),
  ],
})
export class VendordetailPageModule {}
