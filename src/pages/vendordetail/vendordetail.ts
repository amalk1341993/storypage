import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-vendordetail',
  templateUrl: 'vendordetail.html',
})
export class VendordetailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VendordetailPage');
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

  editvendor(){
      let modal = this.modalCtrl.create('EditvendorPage');
      modal.present();
    }

}
