import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FilterbyPage } from './filterby';

@NgModule({
  declarations: [
    FilterbyPage,
  ],
  imports: [
    IonicPageModule.forChild(FilterbyPage),
  ],
})
export class FilterbyPageModule {}
