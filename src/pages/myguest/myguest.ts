import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,Content,Slides,ModalController,PopoverController  } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-myguest',
  templateUrl: 'myguest.html',
})
export class MyguestPage {
  showPrev:any;
  showNext:any;
  currentIndex:any;
  slidertab:any;
  shownGroup = null;

  @ViewChild(Content) content: Content;
  @ViewChild('mySlider') slider: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,public popoverCtrl: PopoverController) {

    let id = this.navParams.get("id");
    this.slidertab = id;
    console.log("id", id);
    setTimeout(() => {
    this.goToSlide(id);
    }, 500)
    this.slidertab = 0;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyguestPage');
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
        this.shownGroup = null;
    } else {
        this.shownGroup = group;
    }
    };
    isGroupShown(group) {
    return this.shownGroup === group;
    };

  goToSlide(id) {
  this.slider.slideTo(id, 500);
  }

  slideChanged() {
  let currentIndex = this.slider.getActiveIndex();
  this.slidertab = currentIndex;
  console.log("Current index is", currentIndex);
  }

  goBack(){
 this.navCtrl.pop();
}

addguest(){
    let modal = this.modalCtrl.create('AddguestPage');
    modal.present();
  }

  presentPopover(myEvent) {
   let popover = this.popoverCtrl.create('EventpopoverPage');
   popover.present({
     ev: myEvent
   });
 }



}
