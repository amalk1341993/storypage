import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyguestPage } from './myguest';

@NgModule({
  declarations: [
    MyguestPage,
  ],
  imports: [
    IonicPageModule.forChild(MyguestPage),
  ],
})
export class MyguestPageModule {}
