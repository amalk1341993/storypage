import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,PopoverController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-form',
  templateUrl: 'form.html',
})
export class FormPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public popoverCtrl: PopoverController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormPage');
  }

  goBack(){
 this.navCtrl.pop();
}

presentPopover(myEvent) {
 let popover = this.popoverCtrl.create('FormfilterPage');
 popover.present({
   ev: myEvent
 });
}

}
