import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyweddingPage } from './mywedding';

@NgModule({
  declarations: [
    MyweddingPage,
  ],
  imports: [
    IonicPageModule.forChild(MyweddingPage),
  ],
})
export class MyweddingPageModule {}
