import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-mywedding',
  templateUrl: 'mywedding.html',
})
export class MyweddingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyweddingPage');
  }

  open_page(page){
  this.navCtrl.push(page);
}

}
