import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IdeainnerPage } from './ideainner';

@NgModule({
  declarations: [
    IdeainnerPage,
  ],
  imports: [
    IonicPageModule.forChild(IdeainnerPage),
  ],
})
export class IdeainnerPageModule {}
