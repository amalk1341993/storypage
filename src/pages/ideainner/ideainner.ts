import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-ideainner',
  templateUrl: 'ideainner.html',
})
export class IdeainnerPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IdeainnerPage');
  }

  open_page(page){
  this.navCtrl.push(page);
  }

  goBack(){
 this.navCtrl.pop();
}

}
