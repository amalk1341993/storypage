import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MybudgetPage } from './mybudget';

@NgModule({
  declarations: [
    MybudgetPage,
  ],
  imports: [
    IonicPageModule.forChild(MybudgetPage),
  ],
})
export class MybudgetPageModule {}
