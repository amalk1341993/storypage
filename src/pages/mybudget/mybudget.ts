import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-mybudget',
  templateUrl: 'mybudget.html',
})
export class MybudgetPage {
  shownGroup = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MybudgetPage');
  }

  goBack(){
 this.navCtrl.pop();
}
toggleGroup(group) {
  if (this.isGroupShown(group)) {
      this.shownGroup = null;
  } else {
      this.shownGroup = group;
  }
  };
  isGroupShown(group) {
  return this.shownGroup === group;
  };

  addexpense(){
      let modal = this.modalCtrl.create('AddexpensePage');
      modal.present();
    }

}
