import { Component } from '@angular/core';
import { IonicPage, NavController,NavParams,ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-moreinfomodal',
  templateUrl: 'moreinfomodal.html',
})
export class MoreinfomodalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoreinfomodalPage');
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

}
