import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoreinfomodalPage } from './moreinfomodal';

@NgModule({
  declarations: [
    MoreinfomodalPage,
  ],
  imports: [
    IonicPageModule.forChild(MoreinfomodalPage),
  ],
})
export class MoreinfomodalPageModule {}
