import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

@IonicPage()

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'MyweddingPage';
  tab2Root = 'VendorPage';
  tab3Root = 'IdeaPage';
  tab4Root = 'MorePage';

  constructor() {

  }
}
