import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-editvendor',
  templateUrl: 'editvendor.html',
})
export class EditvendorPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditvendorPage');
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

}
