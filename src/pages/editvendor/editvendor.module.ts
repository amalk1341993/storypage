import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditvendorPage } from './editvendor';

@NgModule({
  declarations: [
    EditvendorPage,
  ],
  imports: [
    IonicPageModule.forChild(EditvendorPage),
  ],
})
export class EditvendorPageModule {}
