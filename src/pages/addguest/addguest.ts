import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-addguest',
  templateUrl: 'addguest.html',
})
export class AddguestPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddguestPage');
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

}
