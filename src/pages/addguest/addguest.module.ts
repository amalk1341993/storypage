import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddguestPage } from './addguest';

@NgModule({
  declarations: [
    AddguestPage,
  ],
  imports: [
    IonicPageModule.forChild(AddguestPage),
  ],
})
export class AddguestPageModule {}
